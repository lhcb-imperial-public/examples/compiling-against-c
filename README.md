# Compiling against C++

This is an example of a preprepared project using ROOT to make some histograms. It is set up with CMake, to show a "good" project structure.

## Getting started

To compile and use the test program on the imperial machines:
```
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lb-run -c best Urania/v7r0
mkdir build
cd build
cmake ../src
make
./HistWriter ../data/example.root out.pdf
```

You will now have a PDF called out.pdf in the current directory.

Depending on what you want to do, you may want to use a newer version of the Urania environment to use newer versions of ROOT (Urania 7 uses ROOT 6.08). Be aware that different versions of ROOT have different libraries.

## Adding to the code

You will need to add to the code. You can either add new classes, libraries, or programs. After any change you should rerun the cmake ../src command before compiling.

### New classes

To add a new class to an existing library. Add the .cpp and .h files to the appropriate src/library_name/ directory. In the src/library_name/CMakeLists.txt file add the name of the .cpp and .h files to the existing list in the add_library command.

### New libraries

If you wish to add a set of classes of a common theme, you may wish to add them in their own library. To do this make a directory in src/ with the library name. Add a CMakeLists.txt containing the following (example uses NewLibraryName in src/newlibraryname):

```
add_library(NewLibraryName MyClass.cpp MyClass.h MyOtherClass.cpp MyOtherClass.h )

set(NEWLIBRARYNAME_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)

target_include_directories (NewLibraryName PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
                                          ${ROOT_INCLUDE_DIRS})

```

You will need to change NewLibraryName or NEWLIBRARYNAME in the three lines above.

In src/CMakeLists.txt you will need to add the directory containing the new code:
add_subdirectory(newlibraryname)

Add NewLibraryName to the list in the link_libraries command

Add ${NEWLIBRARYNAME_INCLUDE_DIRS} to the list of paths in the include_directories command.


### New programs

To add a new program add the .cpp (and .h) file to the src/progs/ directory (for example NewProgram.cpp). Then add to the src/CMakeLists.txt

```
add_executable(NewProgram progs/NewProgram.cpp)
```

This will tell it to create a new executable called NewProgram from the file NewProgram.cpp
