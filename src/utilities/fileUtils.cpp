#include "fileUtils.h"

#include <TFile.h>
#include <TTree.h>

namespace FileUtils{

TreeFile::TreeFile(const std::string &filename, const std::string &treename, bool update)
{
  fUpdate = update;
  if (fUpdate)
    fFile = new TFile(filename.c_str(), "Update");
  else
    fFile = new TFile(filename.c_str(), "recreate");
  fFileOwner = true;

  fFile->cd();

  fTree = NULL;
  if (fUpdate)
    fTree = static_cast<TTree*>(fFile->Get(treename.c_str()));

  if (!fTree)
    fTree = new TTree(treename.c_str(), treename.c_str());
  

}

TreeFile::TreeFile(TFile *file, const std::string &treename, bool update)
{
  fUpdate = update;
  fFile = file;
  fFileOwner = true;
}


TreeFile::~TreeFile()
{
  close();
//  delete fTree;
  if (fFileOwner)
    delete fFile;
  for (auto it = fAddresses.begin(); it != fAddresses.end(); ++it)
    delete [] it->second;
}

void TreeFile::fill()
{
  fTree->Fill();
}


void TreeFile::close()
{
  if (fFileOwner)
  {
    if (fUpdate)
      fFile->Write("", TObject::kOverwrite);
    else
      fFile->Write();
    fFile->Close();
  }
}

///////////////////////////////////////////////////////////////////////////////
// TreeReader                                                                //
///////////////////////////////////////////////////////////////////////////////

TreeReader::TreeReader(const std::string &file, const std::string &tree):
fTree(tree.c_str())
{
  fTree.Add(file.c_str());
}

TreeReader::TreeReader(const std::vector<std::string> &file, const std::string &tree):
fTree(tree.c_str())
{
  for (size_t i = 0; i < file.size(); ++i)
    fTree.Add(file[i].c_str());
}

TreeReader::TreeReader(const std::vector<std::string> &file, const std::vector<std::string> &tree):
fTree(tree.front().c_str())
{
  if (file.size() != tree.size())
    return;
  for (size_t i = 0; i < file.size(); ++i)
    fTree.AddFile(file[i].c_str(), TTree::kMaxEntries, tree[i].c_str());
}

TreeReader::~TreeReader()
{
  for (auto it = fMdouble.begin(); it != fMdouble.end(); ++it)
    delete [] it->second;
  for (auto it = fMfloat.begin(); it != fMfloat.end(); ++it)
    delete [] it->second;
  for (auto it = fMbool.begin(); it != fMbool.end(); ++it)
    delete [] it->second;
  for (auto it = fMint.begin(); it != fMint.end(); ++it)
    delete [] it->second;
  for (auto it = fMuint.begin(); it != fMuint.end(); ++it)
    delete [] it->second;
  for (auto it = fMuchar.begin(); it != fMuchar.end(); ++it)
    delete [] it->second;
  for (auto it = fMchar.begin(); it != fMchar.end(); ++it)
    delete [] it->second;
  for (auto it = fMshort.begin(); it != fMshort.end(); ++it)
    delete [] it->second;
  for (auto it = fMushort.begin(); it != fMushort.end(); ++it)
    delete [] it->second;
  for (auto it = fMlonglong.begin(); it != fMlonglong.end(); ++it)
    delete [] it->second;
  for (auto it = fMulonglong.begin(); it != fMulonglong.end(); ++it)
    delete [] it->second;
}

size_t TreeReader::getEntries() const
{
  return fTree.GetEntries();
}

void TreeReader::getEntry(size_t entry)
{
  fTree.GetEntry(entry);
}

void TreeReader::addVariable(const std::string &variable, size_t maxlength)
{
  auto branch = fTree.FindBranch(variable.c_str());
  if (!branch)
    throw FileException(variable+" does not exist in tree.");
  std::string title(branch->GetTitle());
  size_t slash = title.find('/');
  char type = title[slash+1];
  size_t arr = title.find('[');
  size_t length = 1;
  if (arr < slash) //it's an array
  {
    length = atoi(&title[0]+arr+1);
    if (length == 0)
      length = maxlength;
    slash = arr;
  }
  switch (type)
  {
    case 'D':
      fMdouble[variable] = new double[length];
      branch->SetAddress(fMdouble[variable]);
      break;
    case 'F':
      fMfloat[variable] = new float[length];
      branch->SetAddress(fMfloat[variable]);
      break;
    case 'O':
      fMbool[variable] = new bool[length];
      branch->SetAddress(fMbool[variable]);
      break;
    case 'I':
      fMint[variable] = new int[length];
      branch->SetAddress(fMint[variable]);
      break;
    case 'i':
      fMuint[variable] = new unsigned int[length];
      branch->SetAddress(fMuint[variable]);
      break;
    case 'B':
      fMchar[variable] = new signed char[length];
      branch->SetAddress(fMchar[variable]);
      break;
    case 'b':
      fMuchar[variable] = new unsigned char[length];
      branch->SetAddress(fMuchar[variable]);
      break;
    case 'S':
      fMshort[variable] = new short[length];
      branch->SetAddress(fMshort[variable]);
      break;
    case 's':
      fMushort[variable] = new unsigned short[length];
      branch->SetAddress(fMushort[variable]);
      break;
    case 'L':
      fMlonglong[variable] = new long long[length];
      branch->SetAddress(fMlonglong[variable]);
      break;
    case 'l':
      fMulonglong[variable] = new unsigned long long[length];
      branch->SetAddress(fMulonglong[variable]);
      break;
    default:
      throw FileException(variable+" is not a supported type.");
  }
}



///////////////////////////////////////////////////////////////////////////////
// Bunch of template specialisations                                         //
///////////////////////////////////////////////////////////////////////////////

template<>
char TypeToLetter<bool>()
{
  return 'O';
}

template<>
char TypeToLetter<float>()
{
  return 'F';
}

template<>
char TypeToLetter<double>()
{
  return 'D';
}

template<>
char TypeToLetter<char>()
{
  return 'B';
}

template<>
char TypeToLetter<unsigned char>()
{
  return 'b';
}

template<>
char TypeToLetter<short>()
{
  return 'S';
}

template<>
char TypeToLetter<unsigned short>()
{
  return 's';
}

template<>
char TypeToLetter<int>()
{
  return 'I';
}

template<>
char TypeToLetter<unsigned int>()
{
  return 'i';
}

template<>
char TypeToLetter<long long>()
{
  return 'L';
}

template<>
char TypeToLetter<unsigned long long>()
{
  return 'l';
}

}
