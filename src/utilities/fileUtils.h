#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <TTree.h>
#include <TChain.h>
class TFile;

#include <string>
#include <sstream>
#include <map>
#include <iostream>
#include <exception>


namespace FileUtils
{

//Exception class for file utils
class FileException: public std::exception
{
  protected:
    std::string fWhat;
  public:
    FileException(const std::string &what):
    fWhat(what)
    {}
    virtual const char* what() const throw()
    {
      return fWhat.c_str();
  }
};

template <typename T> char TypeToLetter()
{
  return 'C';
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  Class to make writing a tree a bit easier                                //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
class TreeFile
{
  public:
    //Constructor to open/create a file filename and add/update a tree treename 
    TreeFile(const std::string &filename, const std::string &treename, bool update=false);
    //Constructor to add/update a tree name tree name in an already opened file
    TreeFile(TFile *file, const std::string &treename, bool update=false);

    ~TreeFile();
    void close();    

    //Adds a branch with a staticly defined length
    template <typename T>
    void addBranch(const std::string &name, size_t length=1)
    {
      T *address = new T[length];
      std::stringstream leaf;
      leaf << name;
      if (length > 1)
        leaf << '[' << length << ']';
      leaf << '/' << TypeToLetter<T>();
      fTree->Branch(name.c_str(), address, leaf.str().c_str());
      fAddresses[name] = static_cast<void*>(address);
    }

    //Adds a branch with a variable length defined in lengthvar, up to maxlength
    template <typename T>
    void addBranch(const std::string &name, const std::string &lengthvar, size_t maxlength=100)
    {
      T *address = new T[maxlength];
      std::stringstream leaf;
      leaf << name << '[' << lengthvar << "]/" << TypeToLetter<T>();
      fTree->Branch(name.c_str(), address, leaf.str().c_str());
    }

    //Sets the value of a specific branch
    template <typename T>
    void setValue(const std::string &name, const T& value)
    {
      auto it = fAddresses.find(name);
      if (it == fAddresses.end())
      {
        std::cerr << "Error: Trying to fill a nonexistant branch: " << name<< std::endl;
        return;
      }
      static_cast<T*>(it->second)[0] = value;
    }

/*    template <typename T>
    void setValue(const std::string &name, const T& begin, const T& end)
    {
      auto it = fAddresses.find(name);
      if (it == fAddresses.end())
      {
        std::cerr << "Error: Trying to fill a nonexistant branch" << std::endl;
        return;
      }
      T 
      while 
      static_cast<T*>(fAddress)[0] = value;
    }
*/
    //fills the tree
    void fill();

  protected:
    TFile *fFile;
    bool fFileOwner;
    TTree *fTree;
    bool fUpdate;
    std::map<std::string, void*> fAddresses;

};

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  Class to make reading a tree a bit easier                                //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
class TreeReader
{
  public:
    //Constructor to open a tree in file
    TreeReader(const std::string &file, const std::string &tree);
    //Constructor to make a chain for files with the same tree name
    TreeReader(const std::vector<std::string> &file, const std::string &tree);
    //Constructor to make a chain of files with associated tree names
    TreeReader(const std::vector<std::string> &file, const std::vector<std::string> &tree);
    ~TreeReader();

    size_t getEntries() const;
    void getEntry(size_t entry);
    template<typename T>
    void get(const std::string &var, T &value, size_t element = 0)
    {
      if (not find(var, value, element))
        addVariable(var);
      find(var, value, element);
    }
    template<typename T>
    T get(const std::string &var)
    {
      T value;
      get(var, value, 0);
      return value;
    }
    void addVariable(const std::string &variable, size_t maxlength = 100);
    inline TChain &tree() {return fTree;}
    inline const TChain &tree() const {return fTree;}
  protected:
    TChain fTree;
    std::map<std::string, double*>             fMdouble;
    std::map<std::string, float*>              fMfloat;
    std::map<std::string, bool*>               fMbool;
    std::map<std::string, int*>                fMint;
    std::map<std::string, unsigned int*>       fMuint;
    std::map<std::string, unsigned char*>      fMuchar;
    std::map<std::string, signed char*>        fMchar;
    std::map<std::string, short*>              fMshort;
    std::map<std::string, unsigned short*>     fMushort;
    std::map<std::string, long long*>          fMlonglong;
    std::map<std::string, unsigned long long*> fMulonglong;

    template <typename T>
    bool find(const std::string &var, T &value, size_t element)
    {
      return !(!find(var, fMdouble,    value, element)
             &&!find(var, fMfloat,     value, element)
             &&!find(var, fMbool,      value, element)
             &&!find(var, fMint,       value, element)
             &&!find(var, fMuint,      value, element)
             &&!find(var, fMuchar,     value, element)
             &&!find(var, fMchar,      value, element)
             &&!find(var, fMshort,     value, element)
             &&!find(var, fMushort,    value, element)
             &&!find(var, fMlonglong,  value, element)
             &&!find(var, fMulonglong, value, element));
    }
    template <typename T1, typename T2>
    bool find(const std::string &var, const T1 &varmap, T2 &value, size_t element)
    {
      auto it = varmap.find(var);
      if (it != varmap.end())
      {
        value = static_cast<T2>(it->second[element]);
        return true;
      }
      return false;
    }
};

///////////////////////////////////////////////////////////////////////////////
// Bunch of template specialisations                                         //
///////////////////////////////////////////////////////////////////////////////

template<>
char TypeToLetter<bool>();

template<>
char TypeToLetter<float>();

template<>
char TypeToLetter<double>();

template<>
char TypeToLetter<char>();

template<>
char TypeToLetter<unsigned char>();

template<>
char TypeToLetter<short>();

template<>
char TypeToLetter<unsigned short>();

template<>
char TypeToLetter<int>();

template<>
char TypeToLetter<unsigned int>();

template<>
char TypeToLetter<long long>();

template<>
char TypeToLetter<unsigned long long>();

}


#endif
