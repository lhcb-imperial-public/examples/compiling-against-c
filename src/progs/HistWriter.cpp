#include <iostream>

#include <TH2D.h>
#include <TCanvas.h>

#include <fileUtils.h>

int main(int argc, char **argv)
{
  //get file name from command line
  if (argc < 2)
  {
    std::cout << "No filename given" << std::endl;
    return -1;
  }
  std::string infilename = argv[1];
  std::cout << "Using file: " << infilename << std::endl;

  //get output file name from command line
  if (argc < 3)
  {
    std::cout << "No output filename given" << std::endl;
    return -1;
  }
  std::string outfilename = argv[2];
  std::cout << "Will save to: " << outfilename << std::endl;
  
  TCanvas c;
  c.SaveAs((outfilename+"[").c_str());

  
  FileUtils::TreeReader tree(infilename, "DataTuple");
  
  tree.tree().Draw("B_M");
  c.SaveAs(outfilename.c_str());

  tree.tree().Draw("B_M", "B_M > 5500");
  c.SaveAs(outfilename.c_str());

  tree.tree().Draw("JPsi_M");
  c.SaveAs(outfilename.c_str());

  TH2D h("masshist", ";m_{B} [GeV];m_{J/#psi} [GeV]", 50, 4.9, 6.0, 50, 2.9, 3.3);
  for (size_t i = 0; i < tree.getEntries(); ++i)
  {
    tree.getEntry(i);
    h.Fill(tree.get<double>("B_M")/1000.0, tree.get<double>("JPsi_M")/1000.0);
  }
  h.Draw("colz");
  c.SaveAs(outfilename.c_str());
  c.SaveAs((outfilename+"]").c_str());
  return 0;
}
